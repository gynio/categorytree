
   $(document).on('click touch', '#categoryTree .btn-add',function(){
        var $this = $(this);
        var li = $this.closest('li');
        var form = li.find('.add-form:last');

        if ($this.hasClass('active')) {
            form.removeClass('active');
            $this.removeClass('active');
        } else {
            form.addClass('active');
            $this.addClass('active');
        }
    });
    
    $(document).on('submit', '#categoryTree .add-form',function(){

        var $this = $(this);
        var form = $this.closest('.add-form');
        var li = form.closest('li');
        var input = form.find('input');
        $.ajax({ 
            url: "/add",
            data: ({ category_tree: ({ category_parent: form.data('id'), category_title: input.val()})}),
            dataType: 'json',
            type: 'post',
            success: function(r){
                if (r.status == 1) {
                    li.find('ul.childs:first').append(r.content);
                    input.val('');
                } else {
                    alert(r.content);
                }
            }
        });
        return false;
    });
 