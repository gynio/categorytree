<?php

namespace App\CategoryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('category_title','text',array(
                'constraints' => array(
                    new Length(array('min' => 1)),
                    new NotBlank(),
                )
            ))
            ->add('category_parent', 'integer',array(
                'constraints' => new NotBlank(),
            ));
    }
    
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }
    
    public function getName()
    {
        return 'category_tree';
    }
}
