<?php
namespace App\CategoryBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAware;  
use Doctrine\ORM\EntityManager;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Form\CategoryType;

class CategoryService extends ContainerAware  
{
    private $em;    
    
    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }
        
    public function addTree($title)
    {
        $category = new Category();
        $category->setTitle($title);
        $this->em->persist($category);
        $this->em->flush();
        
        return $category;
    }
    
    public function getTree($id)
    {
        $repository = $this->em->getRepository('AppCategoryBundle:Category');
        
        if ($id != null) {
            $tree = $repository->findOneById($id);
        } else {
            $tree = $repository->findBy(array('lvl' => 0)); 
        }
        return $tree;
    }
    
    public function getAddForm()
    {
        return new CategoryType();
    }
    
    public function addFormSubmit($data)
    {
       $parent = $this->em->getRepository('AppCategoryBundle:Category')->findOneBy(array(
           'id' => $data['category_parent'],
       ));

       if ($parent != null) {
           $category = new Category();
           $category->setTitle($data['category_title']);
           $category->setParent($parent);
           $this->em->persist($category);
           $this->em->flush();
           
           return $category;
       }
       
       return null;
   }
}
