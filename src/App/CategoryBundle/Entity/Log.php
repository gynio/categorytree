<?php

namespace App\CategoryBundle\Entity;

use Doctrine\ORM\Mapping AS ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\EntityRepository;

/**
 * Log
 * 
 * @ORM\Entity
 * @ORM\Table(name="api_log")
 */
class Log
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(name="requestedUrl", type="string", nullable=true)
     */
    private $requestedUrl;
    
    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    protected $created;
    
    /**
     * @ORM\Column(name="userAgent", type="string", nullable=true)
     */
    private $userAgent;
    
    function getId()
    {
        return $this->id;
    }

    function getRequestedUrl()
    {
        return $this->requestedUrl;
    }

    function getCreated()
    {
        return $this->created;
    }

    function getUserAgent()
    {
        return $this->userAgent;
    }

    function setId($id)
    {
        $this->id = $id;
    }

    function setRequestedUrl($requestedUrl)
    {
        $this->requestedUrl = $requestedUrl;
    }

    function setCreated($created)
    {
        $this->created = $created;
    }

    function setUserAgent($userAgent)
    {
        $this->userAgent = $userAgent;
    }
}
