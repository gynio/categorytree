<?php

namespace App\CategoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\CategoryBundle\Form\CategoryType;

class CategoryController extends Controller
{    
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $service = $this->get('app_category_service', $em);
        
        $category = $service->addTree('Root');
        
        return $this->render('AppCategoryBundle:Category:index.html.twig', array(
            'category_id' => $category->getId(),
            'category_title' => $category->getTitle(),
            'form' => $service->getAddForm()
        ));
    }
    
    public function addAction()
    {
        $em = $this->getDoctrine()->getManager();
        $service = $this->get('app_category_service', $em);
        $request = $this->getRequest();
        
        $json = array(
            'status' => 0,
            'content' => 'Something is wrong!'
        );
        
        $form = $this->createForm($service->getAddForm());
        $form->submit($request);

        if ($form->isValid()) {            
            $category = $service->addFormSubmit($form->getData());
            
            if ($category != null) {
                $json['status'] = 1;
                $json['content'] = $this->renderView('AppCategoryBundle:Category:partial/categoryTreeElement.html.twig', array(
                    'category_id' => $category->getId(),
                    'category_title' => $category->getTitle()
                ));
            }
        } else {
            $json['content'] = $form->getErrorsAsString();
        }
        
        return new JsonResponse($json);
    }
}
