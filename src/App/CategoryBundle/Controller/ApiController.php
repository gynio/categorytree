<?php
namespace App\CategoryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends Controller
{
    public function exportTreeAction($id, $format)
    {      
        $em = $this->getDoctrine()->getManager();
        
        $tree = $this->get('app_category_service', $em)->getTree($id);
        
        if ($tree == null) {
            throw $this->createNotFoundException('No tree category found');
        }
        
        $serializer = \JMS\Serializer\SerializerBuilder::create()->build();
        $jsonContent = $serializer->serialize($tree, $format);
        
        $response = new Response($jsonContent);
        $response->headers->set('Content-Type', 'application/'.$format);

        return $response;
    }
}
