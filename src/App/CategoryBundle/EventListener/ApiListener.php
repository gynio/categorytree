<?php
namespace App\CategoryBundle\EventListener;

use Symfony\Component\EventDispatcher\Event;
use Doctrine\ORM\EntityManager;

class ApiListener
{
    private $em;
    private $event;
        
    /*
     * All routes to log request
     */
    private $loggingRoutes = array(
        'app_category_api_export_tree',
        'app_category_api_export_tree_id'
    );
        
    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }
    
    public function onKernelRequest(Event $event)
    {
        $request = $event->getRequest();
        if (in_array($request->get('_route'), $this->loggingRoutes)) {
            $this->requestLog($request);
        }
    }
    
    public function requestLog($request)
    {        
        $log = new \App\CategoryBundle\Entity\Log();
        $log->setRequestedUrl($request->getUri());
        $log->setCreated(new \DateTime('now'));
        $log->setUserAgent($request->headers->get('User-Agent'));
        
        $this->em->persist($log);
        $this->em->flush();
    }
}