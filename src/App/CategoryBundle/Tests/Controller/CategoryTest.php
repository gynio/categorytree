<?php
namespace App\CategoryBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoryTest extends WebTestCase
{
    public function testCategoryIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertCount(1, $crawler->filter('h1'));
    }
    
    public function testCategoryAddAction()
    {
        $client = static::createClient();      
        $client->request('POST', '/add', array('category_tree' => array('category_title' => 'PHP unit test', 'category_parent' => 1)));
        
        $responseContent = json_decode((string) $client->getResponse()->getContent());
        $this->assertTrue(property_exists($responseContent,'status') && $responseContent->status == 1);
    }
    
    public function testApi()
    {
        $client = static::createClient();      
        $client->request('GET', '/api/category_tree/1.xml');        
        $this->assertEquals(
            200,
            $client->getResponse()->getStatusCode()
        );
        
        $client = static::createClient();      
        $client->request('GET', '/api/category_tree/1.sql');        
        $this->assertEquals(
            404,
            $client->getResponse()->getStatusCode()
        );
    }
}
